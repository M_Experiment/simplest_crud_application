-- Sample employee database
-- Table Department
--
-- Original data created by Fusheng Wang and Carlo Zaniolo
-- http://timecenter.cs.aau.dk/software.htm
-- http://timecenter.cs.aau.dk/Data/employeeTemporalDataSet.zip
--
-- Giuseppe Maxia made the relational schema and Patrick Crews
-- exported the data in relational format.
--
-- The database contains about 300,000 employee records
-- with 2.8 million salary entries.
--
-- This work is licensed under the
-- Creative Commons Attribution-Share Alike 3.0 Unported License.
-- To view a copy of this license, visit
-- http://creativecommons.org/licenses/by-sa/3.0/ or send a letter to
-- Creative Commons, 171 Second Street, Suite 300, San Francisco,
-- California, 94105, USA.
--
-- DISCLAIMER
-- To the best of our knowledge, this data is fabricated,
-- and it does not correspond to real people.
-- Any similarity to existing people is purely coincidental.

CREATE TABLE kzt0001 (
    id             NUMERIC(15)         NOT NULL,
    short_name     VARCHAR(4)          NOT NULL,
    long_name      VARCHAR(40)         NOT NULL,
    created_by     VARCHAR(8)          NOT NULL,
    created_at     TIMESTAMP           NOT NULL,
    changed_by     VARCHAR(8)          NOT NULL,
    changed_at     TIMESTAMP           NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT kzi000101 UNIQUE (short_name)
);

COMMENT ON TABLE kzt0001 IS 'Department';

INSERT INTO kzt0001 VALUES
    (1, 'D001','Marketing', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (2, 'D002','Finance', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (3, 'D003','Human Resources', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (4, 'D004','Production', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (5, 'D005','Development', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (6, 'D006','Quality Management', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (7, 'D007','Sales', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (8, 'D008','Research', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363'),
    (9, 'D009','Customer Service', 'USER', '2020-04-07 09:28:15.687363', 'USER', '2020-04-07 09:28:15.687363');

COMMIT;
