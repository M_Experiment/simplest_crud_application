import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';

import { DepartmentService } from '../department.service';
import { Department } from '../department';

import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-department-list',
  templateUrl: './department-list.component.html',
  styleUrls: ['./department-list.component.css'],
})
export class DepartmentListComponent implements OnInit {
  displayedColumns: string[] = [
    'shortName',
    'longName',
    'createdBy',
    'createdAt',
    'changedBy',
    'changedAt',
    'id',
  ];
  data: Department[] = [];
  dataSource: MatTableDataSource<Department>;
  //sort: MatSort;   //--> not working!
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  isLoadingResults = true;
  selectedRowIndex: number;

  constructor(private api: DepartmentService, private title: Title) {}

  ngOnInit(): void {
    //TO-DO: better use TitleService & Router instead of following..
    this.title.setTitle('Department');

    this.api.list().subscribe(
      (res: any) => {
        this.data = res;
        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.sort = this.sort;
        console.log(this.data);
        this.isLoadingResults = false;
      },
      (err) => {
        console.log(err);
        this.isLoadingResults = false;
      }
    );
  }
}
