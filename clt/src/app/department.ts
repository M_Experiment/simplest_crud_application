export class Department {
  id: number;
  shortName: string;
  longName: string;
  createdBy: string;
  createdAt: Date;
  changedBy: string;
  changedAt: Date;
 }
