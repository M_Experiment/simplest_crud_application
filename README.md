# Simplest_CRUD_Application

## UNDER CONSTRUCTION!
This software is currently being developed and is far from being finished.

## Description
An Example for simple CRUD Application with Angular clients and Java-Spring servers using PostgreSQL database.

## Attention! 
This software is currently being developed by newbies and has not yet been verified by experts. Therefore please do not use it as a reference!

## Licence
This work is licensed under the Creative Commons Attribution-Share Alike 3.0 Unported License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/
or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco California, 94105, USA.