package com.server.srv.repository;

import com.server.srv.model.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface DepartmentRepository extends JpaRepository<Department, Long> {

    List<Department> findByOrderByShortNameAsc();

}
