package com.server.srv.model;

import lombok.Data;
import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "KZT0001")
public class Department {

  // No need of getter&setter thanks to lombok

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  private String shortName;

  private String longName;

  private String createdBy;

  @Temporal(TemporalType.TIMESTAMP)
  private Date createdAt;

  private String changedBy;

  @Temporal(TemporalType.TIMESTAMP)
  private Date changedAt;

}